```
USAGE
       box [option]

OPTIONS
          get [box]
              Gets a copy of an existing package definition, aka box.

          set [summary]
              Sets changes made on the current dir to the server. Optionally provide a [summary] of them. Oth‐
              erwise a timestamp is used instead, which shall be good enough for nearly all cases.

          new [box]
              Creates a new box.

          pack
              Packages the box on the current dir.

          web [box]
              Opens the webpage of the box. If [box] isn't provided the current dir is assumed.

          wiki
              Opens the wiki documentation on creating boxes.

          key [-override] [user]
              Shows the key necessary for accessing the server. If [-override] is included, any  existing  key
              is  overriden  by  a new one. [user] is the user at <https://aur.archlinux.org>, if that matches
              your computer user name it can be ommitted.
```
